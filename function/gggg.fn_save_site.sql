CREATE OR REPLACE FUNCTION gggg.fn_save_site(
	p_siteCode in character varying,
	p_siteName in character varying,
	p_formId in bigint,
	p_isActive in character varying,
	p_createdBy in character varying
 )
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
	vc 	     bigint; 
	v_result bigint;
	v_siteId bigint; 

	v_timestamp timestamp;  

 --  v_result : 
 --	 0		-> not valid 
 --	 1.. 	-> userid 
 -- -1		-> already exists 	

begin
		SET TIMEZONE='Asia/Jakarta';	 
	 	v_timestamp = now(); 
	 	 						
	 	 -- not exists 
	 	 if not exists(
			 	select *  
			 	from gggg.m_site ms 
			 	where ms.sitecode = p_siteCode
		 ) then	 	
				insert into gggg.m_site(						
						 sitecode 
						,sitename 
						,isactive 
						,formaccessmentid 
						,creation_date
						,last_update_date 				
						,created_by 
						,last_updated_by 
				)
				values(
					    p_siteCode
					   ,p_siteName
					   ,p_isActive
					   ,p_formId
					   ,v_timestamp
					   ,v_timestamp
					   ,p_createdBy
					   ,p_createdBy 
				)
				returning siteId into v_siteId;
				v_result = v_siteId;	
	 	else	 	
		 		-- update if existing 
	 			update gggg.m_site
	 			set  sitename 			= p_siteName, 
				 	 isactive			= p_isActive, 
				 	 formaccessmentid	= p_formId, 
				 	 last_update_date 	= v_timestamp,		
				 	 last_updated_by    = p_createdBy 
	 			where sitecode = p_siteCode 
	 			returning siteid into v_siteId;
	 	
		 		v_result = v_siteId;	 		
	 	end if; 

	    -- set output var 
    	return  v_result; 	
END;
$function$
;