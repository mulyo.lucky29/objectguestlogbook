CREATE OR REPLACE FUNCTION gggg.fn_change_password(
	p_username in character varying,
	p_password in character varying,
	p_createdBy in character varying
 )
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
	vc 	     bigint; 
	v_result bigint;
	v_userId bigint; 
	v_timestamp timestamp;  

begin
		SET TIMEZONE='Asia/Jakarta';	 
	 	v_timestamp = now(); 
	 
	 	-- not exists 
	 	if exists(
		 	select *  
		 	from gggg.m_login ml 
		 	where ml.username = p_username
		) then	 	
			update gggg.m_login
			set encryptedpassword = md5(p_password), 
				last_update_date  = v_timestamp,
				last_updated_by   = p_createdBy 
			where username = p_username
		    returning userId into v_userId;
			
		    v_result = v_userId;	
	 	else	 	
	 		v_result = -1;	 	
	 	end if; 

	    -- set output var 
    	return  v_result; 	
END;
$function$
;