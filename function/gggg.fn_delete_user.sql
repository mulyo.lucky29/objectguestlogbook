CREATE OR REPLACE FUNCTION gggg.fn_delete_user(p_userid in bigint)
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
	v_userid bigint; 
 begin
		 SET TIMEZONE='Asia/Jakarta';	 

	 	if(p_userid > 0) then 				
			-- delete 
			delete from gggg.m_login 
			where userid = p_userid
			returning userid into v_userid;		
		else 								
			v_userid = -1;	
		end if; 
       
	    -- set output var 
    	return  v_userid; 	
END;
$function$
;

