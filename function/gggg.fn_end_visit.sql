CREATE OR REPLACE FUNCTION gggg.fn_end_visit(
	 p_endvisitid in bigint,	 
	 p_endvisitreason in character varying,
	 p_endvisitby in character varying 
 )
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
 	v_visitid	bigint; 
	v_timestamp timestamp; 
 begin
	 SET TIMEZONE='Asia/Jakarta';

	 v_timestamp = now(); 
						
	  if exists(
		 select visitid
		 from gggg.m_visit
		 where visitid = p_endvisitid
		   and endvisitflag is null 
	  ) then 
		  update gggg.m_visit
		  set endvisitdate   = v_timestamp,
		  	  endvisitflag   = 'Y',
		  	  endvisitreason = p_endvisitreason,
		  	  endvisitby     = p_endvisitby,
		  	  last_update_date = v_timestamp
		  where visitid = p_endvisitid	
 		    and endvisitflag is null 
		  returning visitid into v_visitid;	 
	   else
	  		v_visitid = -1; 
	   end if; 
	  
	 -- set output var 
     return  v_visitid; 	
END;
$function$
;
