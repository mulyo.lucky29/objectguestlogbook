CREATE OR REPLACE FUNCTION gggg.fn_save_visit(
	 p_siteid in bigint,	 
	 p_guestid in bigint,	 
	 p_contactid in bigint,
	 p_visitpurpose in character varying,
	 p_contactaltname in character varying,
	 p_assuranceCardType in character varying,
	 p_assuranceCardNo in character varying,
	 p_formaccessment in character varying
 )
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
	v_visitid bigint; 
	v_session_id varchar(50);
	v_timestamp timestamp; 
	v_visitdate date; 
	v_sitecode varchar(50);
	v_sitename varchar(50); 	 	  
	v_guestcardid varchar(50);
	v_guestname varchar(500);
	v_guestphone varchar(50);
	v_guestemail varchar(500);
	v_guestcompany varchar(500);
	v_guestpictureurl varchar(2500);
	v_guestpicturecardid varchar(2500);
	v_contactname varchar(500);
	v_contactemail varchar(500);
	v_contactext varchar(50);		
	v_sitespecific varchar(500); 
	v_formaccessmentid bigint; 

 begin
	 SET TIMEZONE='Asia/Jakarta';
							
	 -- get info site 
	 select sitecode,
	 	    sitename,
	 	    formaccessmentid 
	 into v_sitecode,
	 	  v_sitename,
	 	  v_formaccessmentid 
	 from gggg.m_site
	 where siteid = p_siteid; 

	 -- get id session timestamp generated 
	 select MD5(v_sitecode || TO_CHAR(now(), 'yyyy-mm-dd HH24:MI:SS:MS.US')),
	 	   date_trunc('day', now()),
	 	   now()
	 into v_session_id, 
	 	  v_visitdate, 
	 	  v_timestamp; 
	 	  
	 -- get info guest by guest id 
	 select guestcardid,
	 		guestname,
	 		guestphone,
	 		guestemail,
	 		guestcompany,
	 		guestpictureurl,
	 		guestpicturecardid 
	 into v_guestcardid,
		  v_guestname, 
	 	  v_guestphone,
	 	  v_guestemail,
	 	  v_guestcompany,
	 	  v_guestpictureurl,
	 	  v_guestpicturecardid
	 from gggg.m_guest
	 where guestid = p_guestid; 	
	
	 -- get info contact by contact
	 if(p_contactid > 0) then 
		select contactname,
			   contactemail,
			   contactext,
			   sitespecific 
		into v_contactname,
			 v_contactemail,
			 v_contactext,
			 v_sitespecific 		
		from gggg.m_contact
		where contactid  = p_contactid; 
	 else
		-- kalo contact id bukan dari list (contactid = -1)
		v_contactname 	= null;
		v_contactemail	= null; 
		v_contactext	= null; 	
		v_sitespecific	= null; 
	 end if; 
	
	 insert into gggg.m_visit(
	 	visitsessioncode,	--0
		visitdate,			--1
		visitpurpose,		--2
		siteid,				--3
		sitecode,			--4
		sitename,			--5
		guestid,			--6
		guestcardid,		--7
		guestname,			--8
		guestphone,			--9
		guestemail,			--10
		guestcompany,		--11
		guestpictureurl,	--12
		guestpicturecardid,	--13
		contactid,			--14
		contactname,		--15
		contactemail,		--16
		contactext,			--17
		sitespecific,		--18
		formaccessmentid,   --19
		formaccessment, 	--20
		contactaltname,		--21
		assurancecardtype,  --22
		assurancecardno,    --23
		creation_date,		--24
		last_update_date 	--25
	 )
	 values(
	 	v_session_id,			--0
	 	v_visitdate,			--1
		p_visitpurpose,			--2
		p_siteid,				--3
		v_sitecode,				--4
		v_sitename,				--5
		p_guestid,				--6
		v_guestcardid,			--7
		v_guestname,			--8
		v_guestphone,			--9
		v_guestemail,			--10
		v_guestcompany,			--11
		v_guestpictureurl,		--12
		v_guestpicturecardid,	--13
		p_contactid,			--14
		v_contactname,			--15
		v_contactemail,			--16
		v_contactext,			--17
		v_sitespecific,			--18
		v_formaccessmentid,		--19
		to_json(p_formaccessment::json), --20
		p_contactaltname,		--21
		p_assuranceCardType,	--22
		p_assuranceCardNo,		--23
		v_timestamp,			--24
		v_timestamp				--25		
	 )
	 returning visitid into v_visitid;	 
	
	 -- set output var 
     return  v_visitid; 	
END;
$function$
;
