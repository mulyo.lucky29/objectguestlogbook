CREATE OR REPLACE FUNCTION gggg.fn_delete_contact(p_contactid in bigint)
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
	v_contactid bigint; 
 begin
		 SET TIMEZONE='Asia/Jakarta';	 

	 	if(p_contactid > 0) then 				
			-- delete 
			delete from gggg.m_contact 
			where contactId = p_contactid
			returning contactId into v_contactid;		
		else 								
			v_contactid = -1;	
		end if; 
       
	    -- set output var 
    	return  v_contactid; 	
END;
$function$
;
