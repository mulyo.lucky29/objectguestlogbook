CREATE OR REPLACE FUNCTION gggg.fn_do_checkinout( 	 
	 p_session_code character varying, 
	 p_terminal_id  character varying	 
 )
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
	v_timestamp timestamp; 
	v_checklogid bigint; 
	v_visitid bigint;
	v_visitsessioncode varchar(50);
	v_checkin timestamp;
	v_checkout timestamp; 
 begin
		 SET TIMEZONE='Asia/Jakarta';

		 v_timestamp = now(); 

		if exists(
			 -- find visit session which is not ended already 
			 select visitid
			 from gggg.m_visit
			 where visitsessioncode = p_session_code
			  and endvisitdate is null 
		) then 
	 		 -- get info from visit 
			 select visitid, 
			 	    visitsessioncode,
			 	    checkin,
			 	    checkout 
			 into v_visitid, 
			 	  v_visitsessioncode,
			 	  v_checkin,
			 	  v_checkout 
			 from gggg.m_visit
			 where visitsessioncode = p_session_code; 
									
		 	 -- insert into table 
			 insert into gggg.m_checklog(
					visitid, 
					visitsessioncode, 
					checkedtime, 
					checkedterminalid
			)
			values(
				v_visitid,
				v_visitsessioncode, 
				v_timestamp,
				p_terminal_id 
			)
			returning checklogid into v_checklogid;	 
				
			-- to create in/out reproxically 
			IF(v_checkin is null and v_checkout is null) then 	
				begin 
					update gggg.m_visit
					set checkin 		 = v_timestamp,
						last_update_date = v_timestamp 
					where visitsessioncode = p_session_code;
				
					update gggg.m_checklog
					set checktyped = 'CHECK-IN'
					where checklogid = v_checklogid; 
				end;
			elseif(v_checkin is not null and v_checkout is null) then 
				begin 
					update gggg.m_visit
					set checkout = v_timestamp, 
						last_update_date = v_timestamp 
					where visitsessioncode = p_session_code;

					update gggg.m_checklog
					set checktyped = 'CHECK-OUT'
					where checklogid = v_checklogid; 
				end;	
			elseif(v_checkin is not null and v_checkout is not null) then 
				begin 
					-- if previously both already filled then only fill check in and null to checkout
					update gggg.m_visit
					set checkin 		 = v_timestamp,
						checkout 		 = null,
						last_update_date = v_timestamp 
					where visitsessioncode = p_session_code;					

					update gggg.m_checklog
					set checktyped = 'CHECK-IN'
					where checklogid = v_checklogid; 				
				end;
			end if; 
		else
			-- visit id not detected then reject checklog id 
			v_checklogid = -1;		
		end if; 
		  
	
	 -- set output var 
     return  v_checklogid; 	
END;
$function$
;
