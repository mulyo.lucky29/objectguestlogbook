CREATE OR REPLACE FUNCTION gggg.fn_login(
	p_username in character varying,
	p_password in character varying	
 )
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
	vc 	     bigint; 
	v_result bigint;
	v_user_id bigint; 
	v_encrypted_password varchar(50);
	v_encrypted_pwd varchar(50);
	v_timestamp timestamp;  

 --  v_result : 
 --	 0		-> not found 
 --	 1.. 	-> userid 
 -- -1		-> not valid  	

begin
		 SET TIMEZONE='Asia/Jakarta';	 
	 	 v_timestamp 	 = now(); 	 
		 v_encrypted_pwd = md5(p_password ::varchar(50)); 
	
	 	 if exists(
			 	select * 
			 	from gggg.m_login ml 
			 	where ml.username = p_username	 	
	 	 ) then
		 		-- check for password entered matched to encrypted password 
		 		select userid, encryptedpassword 
		 		into v_user_id, v_encrypted_password 
		 		from gggg.m_login ml 
		 		where ml.username = p_username; 
		 		
		 		if(v_encrypted_password != v_encrypted_pwd) then		 				 							
		 			-- login not valid 
		 			v_result = -1;		
		 		else 
		 			-- update last login date 
		 			update gggg.m_login 
		 			set last_login_date  = v_timestamp,
		 				last_update_date = v_timestamp
		 			where username = p_username
		 			returning userid into v_user_id;
		 		
		 			-- login valid return userid 
		 			v_result = v_user_id; 		 		 		
		 		end if;		 		
		 else 
		 	-- username not found 
		 	v_result = 0; 
		 end if;
	 	       
	    -- set output var 
    	return  v_result; 	
END;
$function$
;