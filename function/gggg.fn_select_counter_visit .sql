CREATE OR REPLACE FUNCTION gggg.fn_select_counter_visit(p_site_id integer, p_visit_date character varying)
 RETURNS TABLE(sc_siteid bigint, sc_sitecode character varying, sc_sitename character varying, sc_visitdate date, sc_person_visit bigint, sc_person_active_visit bigint, sc_person_ended_normal_visit bigint, sc_person_ended_cancelled_visit bigint, sc_person_visit_expired bigint, sc_session_visit bigint, sc_session_active_visit bigint, sc_session_ended_normal_visit bigint, sc_session_ended_cancelled_visit bigint, sc_session_visit_expired bigint)
 LANGUAGE plpgsql
AS $function$
DECLARE
BEGIN
        
        RETURN QUERY
        select sc.siteid :: bigint				     		     SITEID, 
        	   ms.sitecode :: varchar(50)						 SITECODE,
        	   ms.sitename :: varchar(50)						 SITENAME, 
			   sc.visitdate :: date				     		     VISITDATE, 
			   sc.PERSON_VISIT :: bigint	     		     	 PERSON_VISIT,
			   sc.PERSON_ACTIVE_VISIT :: bigint  		     	 PERSON_ACTIVE_VISIT,
			   sc.PERSON_ENDED_NORMAL_VISIT :: bigint      		 PERSON_ENDED_NORMAL_VISIT,
			   sc.PERSON_ENDED_CANCELLED_VISIT :: bigint   		 PERSON_ENDED_CANCELLED_VISIT,
			   sc.PERSON_VISIT_EXPIRED :: bigint 				 PERSON_VISIT_EXPIRED, 
			   sc.SESSION_VISIT :: bigint 			     		 SESSION_VISIT,
			   sc.SESSION_ACTIVE_VISIT :: bigint 		     	 SESSION_ACTIVE_VISIT,
			   sc.SESSION_ENDED_NORMAL_VISIT :: bigint     		 SESSION_ENDED_NORMAL_VISIT,
			   sc.SESSION_ENDED_CANCELLED_VISIT :: bigint  		 SESSION_ENDED_CANCELLED_VISIT,
			   sc.SESSION_VISIT_EXPIRED :: bigint 				 SESSION_VISIT_EXPIRED 
        from (
	        select dty.siteid   				     		     	   SITEID, 
			       dty.visitdate 					     		       VISITDATE, 
				   coalesce(sum(dty.PERSON_VISIT),0)     		       PERSON_VISIT,
				   coalesce(sum(dty.PERSON_ACTIVE_VISIT),0)		       PERSON_ACTIVE_VISIT,
				   coalesce(sum(dty.PERSON_ENDED_NORMAL_VISIT),0)      PERSON_ENDED_NORMAL_VISIT,
				   coalesce(sum(dty.PERSON_ENDED_CANCELLED_VISIT),0)   PERSON_ENDED_CANCELLED_VISIT,
				   coalesce(sum(dty.PERSON_VISIT_EXPIRED),0)		   PERSON_VISIT_EXPIRED,
				   coalesce(sum(dty.SESSION_VISIT),0)  			       SESSION_VISIT,
				   coalesce(sum(dty.SESSION_ACTIVE_VISIT),0)	       SESSION_ACTIVE_VISIT,
				   coalesce(sum(dty.SESSION_ENDED_NORMAL_VISIT),0)     SESSION_ENDED_NORMAL_VISIT,
				   coalesce(sum(dty.SESSION_ENDED_CANCELLED_VISIT),0)  SESSION_ENDED_CANCELLED_VISIT,
				   coalesce(sum(dty.SESSION_VISIT_EXPIRED),0) 		   SESSION_VISIT_EXPIRED
			from (
				-- person visit total (cancelled not included)
				select dtx.siteid     siteid,
				       dtx.visitdate  visitdate, 
					   count(*) 	  PERSON_VISIT,
					   0			  PERSON_ACTIVE_VISIT,
					   0			  PERSON_ENDED_NORMAL_VISIT,
					   0			  PERSON_ENDED_CANCELLED_VISIT,
					   0			  PERSON_VISIT_EXPIRED,
					   0			  SESSION_VISIT,
					   0			  SESSION_ACTIVE_VISIT,
					   0			  SESSION_ENDED_NORMAL_VISIT,
					   0			  SESSION_ENDED_CANCELLED_VISIT,
					   0			  SESSION_VISIT_EXPIRED
				from (
					select siteid,
						   visitdate, 	   
						   guestid,
						   guestcardid    
					from gggg.m_visit
					where 1 = 1 
					  and visitdate = TO_DATE(p_visit_date,'YYYY-MM-DD')
					  and upper(coalesce(endvisitreason,'')) <> 'CANCELLED'
					group by siteid, visitdate, guestid ,guestcardid 
				) dtx 
				group by dtx.siteid, dtx.visitdate 
				
				union all 
				
				-- person visit normal active 
				select dtx.siteid	  siteid, 
					   dtx.visitdate  visitdate, 				   
					   0			  PERSON_VISIT,
					   count(*) 	  PERSON_ACTIVE_VISIT,
					   0			  PERSON_ENDED_NORMAL_VISIT,
					   0			  PERSON_ENDED_CANCELLED_VISIT,
					   0			  PERSON_VISIT_EXPIRED,					   
					   0			  SESSION_VISIT,
					   0			  SESSION_ACTIVE_VISIT,
					   0			  SESSION_ENDED_NORMAL_VISIT,
					   0			  SESSION_ENDED_CANCELLED_VISIT,
					   0			  SESSION_VISIT_EXPIRED					   
				from (
					select siteid,
						   visitdate, 	   
						   guestid,
						   guestcardid 
					from gggg.m_visit
					where 1 = 1
					  and visitdate = TO_DATE(p_visit_date,'YYYY-MM-DD')
					  and endvisitdate is null 
					group by siteid, visitdate, guestid ,guestcardid 
				) dtx 
				group by dtx.siteid, dtx.visitdate 
				
				union all 
				
				-- person visit normal ended 
				select dtx.siteid     siteid,
					   dtx.visitdate  visitdate,
					   0			  TOTAL_PERSON_VISIT,
					   0			  TOTAL_PERSON_ACTIVE_VISIT,
					   count(*)	      TOTAL_PERSON_ENDED_NORMAL_VISIT, 
					   0			  TOTAL_PERSON_ENDED_CANCELLED_VISIT,
					   0			  PERSON_VISIT_EXPIRED,							   
					   0			  SESSION_VISIT,
					   0			  SESSION_ACTIVE_VISIT,
					   0			  SESSION_ENDED_NORMAL_VISIT,
					   0			  SESSION_ENDED_CANCELLED_VISIT,
					   0			  SESSION_VISIT_EXPIRED						   
				from (
					select siteid,
						   visitdate, 	   
						   guestid,
						   guestcardid 
					from gggg.m_visit
					where 1 = 1
					  and visitdate = TO_DATE(p_visit_date,'YYYY-MM-DD')
					  and endvisitdate is not null 
					  and upper(coalesce(endvisitreason,'')) = ''
					group by siteid, visitdate, guestid ,guestcardid 
				) dtx 
				group by dtx.siteid, dtx.visitdate 
				
				union all 
				
				-- person visit cancel ended 
				select  dtx.siteid    siteid,
				        dtx.visitdate visitdate, 			         
					    0			  TOTAL_PERSON_VISIT,
					    0			  TOTAL_PERSON_ACTIVE_VISIT,
				 	    0			  TOTAL_PERSON_ENDED_NORMAL_VISIT,	    
					    count(*)      TOTAL_PERSON_ENDED_CANCELLED_VISIT,
 					    0			  PERSON_VISIT_EXPIRED,						    
					    0			  SESSION_VISIT,
					    0			  SESSION_ACTIVE_VISIT,
					    0			  SESSION_ENDED_NORMAL_VISIT,
					    0			  SESSION_ENDED_CANCELLED_VISIT,
 					    0			  SESSION_VISIT_EXPIRED						    
				from (
					select siteid,
						   visitdate, 	   
						   guestid,
						   guestcardid 
					from gggg.m_visit
					where 1 = 1
					  and visitdate = TO_DATE(p_visit_date,'YYYY-MM-DD')
					  and endvisitdate is not null 
					  and upper(coalesce(endvisitreason,'')) = 'CANCELLED'
					group by siteid, visitdate, guestid ,guestcardid 
				) dtx 
				group by dtx.siteid, dtx.visitdate 
				
				union all 
				
				-- person visit expired 
				select  dtx.siteid    siteid,
				        TO_DATE(p_visit_date,'YYYY-MM-DD') visitdate, 			         
					    0			  TOTAL_PERSON_VISIT,
					    0			  TOTAL_PERSON_ACTIVE_VISIT,
				 	    0			  TOTAL_PERSON_ENDED_NORMAL_VISIT,	    
					    0		      TOTAL_PERSON_ENDED_CANCELLED_VISIT,
 					    count(*)      PERSON_VISIT_EXPIRED,						    
					    0			  SESSION_VISIT,
					    0			  SESSION_ACTIVE_VISIT,
					    0			  SESSION_ENDED_NORMAL_VISIT,
					    0			  SESSION_ENDED_CANCELLED_VISIT,
 					    0			  SESSION_VISIT_EXPIRED						    
				from (
					select siteid,
						   visitdate, 	   
						   guestid,
						   guestcardid 
					from gggg.m_visit
					where 1 = 1					
					   and endvisitdate is null 
				       and visitdate < ((now() - interval '1 DAY') ::DATE)
					group by siteid, visitdate, guestid ,guestcardid 
				) dtx 
				group by dtx.siteid 
								
				union all 
				
				-- visit session total (cancelled not included)
				select dtx.siteid     siteid,
				       dtx.visitdate  visitdate, 
					   0		 	  PERSON_VISIT,
					   0			  PERSON_ACTIVE_VISIT,
					   0			  PERSON_ENDED_NORMAL_VISIT,
					   0			  PERSON_ENDED_CANCELLED_VISIT,
				       0			  PERSON_VISIT_EXPIRED,						   
					   count(*)  	  SESSION_VISIT,
					   0			  SESSION_ACTIVE_VISIT,
					   0			  SESSION_ENDED_NORMAL_VISIT,
					   0			  SESSION_ENDED_CANCELLED_VISIT,
				       0			  SESSION_VISIT_EXPIRED						   
				from gggg.m_visit dtx
				where 1 = 1 
					and visitdate = TO_DATE(p_visit_date,'YYYY-MM-DD')
					and upper(coalesce(dtx.endvisitreason,'')) <> 'CANCELLED'
				group by dtx.siteid, dtx.visitdate 
							
				union all 
				
				-- visit session normal active 
				select dtx.siteid	  siteid, 
					   dtx.visitdate  visitdate, 				   
					   0			  PERSON_VISIT,
					   0		 	  PERSON_ACTIVE_VISIT,
					   0			  PERSON_ENDED_NORMAL_VISIT,
					   0			  PERSON_ENDED_CANCELLED_VISIT,
				       0			  PERSON_VISIT_EXPIRED,							   
					   0			  SESSION_VISIT,
					   count(*)		  SESSION_ACTIVE_VISIT,
					   0			  SESSION_ENDED_NORMAL_VISIT,
					   0			  SESSION_ENDED_CANCELLED_VISIT,
				       0			  SESSION_VISIT_EXPIRED						   
				from gggg.m_visit dtx
				where 1 = 1
				  and visitdate = TO_DATE(p_visit_date,'YYYY-MM-DD')
				  and dtx.endvisitdate is null 
				group by dtx.siteid, dtx.visitdate 
				
				union all 
				
				-- visit session normal ended 
				select dtx.siteid     siteid,
					   dtx.visitdate  visitdate,
					   0			  TOTAL_PERSON_VISIT,
					   0			  TOTAL_PERSON_ACTIVE_VISIT,
					   0		      TOTAL_PERSON_ENDED_NORMAL_VISIT, 
					   0			  TOTAL_PERSON_ENDED_CANCELLED_VISIT,
				       0			  PERSON_VISIT_EXPIRED,						   
					   0			  SESSION_VISIT,
					   0			  SESSION_ACTIVE_VISIT,
					   count(*)		  SESSION_ENDED_NORMAL_VISIT,
					   0			  SESSION_ENDED_CANCELLED_VISIT,
				       0			  SESSION_VISIT_EXPIRED						   
				from gggg.m_visit dtx
				where 1 = 1
				  and visitdate = TO_DATE(p_visit_date,'YYYY-MM-DD')
				  and dtx.endvisitdate is not null 
				  and upper(coalesce(dtx.endvisitreason,'')) = ''
				group by dtx.siteid, dtx.visitdate 
							
				union all 
				
				-- visit session cancel ended 
				select  dtx.siteid    siteid,
				        dtx.visitdate visitdate, 			         
					    0			  TOTAL_PERSON_VISIT,
					    0			  TOTAL_PERSON_ACTIVE_VISIT,
				 	    0			  TOTAL_PERSON_ENDED_NORMAL_VISIT,	    
					    0		      TOTAL_PERSON_ENDED_CANCELLED_VISIT,
				        0			  PERSON_VISIT_EXPIRED,							    
					    0			  SESSION_VISIT,
					    0			  SESSION_ACTIVE_VISIT,
					    0			  SESSION_ENDED_NORMAL_VISIT,
					    count(*)	  SESSION_ENDED_CANCELLED_VISIT,
	 			        0			  SESSION_VISIT_EXPIRED					    
				from gggg.m_visit dtx 
				where 1 = 1
				  and visitdate = TO_DATE(p_visit_date,'YYYY-MM-DD')
				  and dtx.endvisitdate is not null 
				  and upper(coalesce(dtx.endvisitreason,'')) = 'CANCELLED'
				group by dtx.siteid, dtx.visitdate 			
				
				union all 
				
				-- visit session expired 
				select  dtx.siteid    siteid,
				        TO_DATE(p_visit_date,'YYYY-MM-DD') visitdate, 			         
					    0			  TOTAL_PERSON_VISIT,
					    0			  TOTAL_PERSON_ACTIVE_VISIT,
				 	    0			  TOTAL_PERSON_ENDED_NORMAL_VISIT,	    
					    0		      TOTAL_PERSON_ENDED_CANCELLED_VISIT,
				        0			  PERSON_VISIT_EXPIRED,							    
					    0			  SESSION_VISIT,
					    0			  SESSION_ACTIVE_VISIT,
					    0			  SESSION_ENDED_NORMAL_VISIT,
					    0        	  SESSION_ENDED_CANCELLED_VISIT,
	 			        count(*)	  SESSION_VISIT_EXPIRED		
	 			from gggg.m_visit dtx 
				where 1 = 1
				  and dtx.endvisitdate is null 
				  and visitdate < ((now() - interval '1 DAY') ::DATE)
				group by dtx.siteid 					
				
			) dty 		
			where dty.siteid    = p_site_id 
			group by dty.siteid, dty.visitdate 
	)sc left join gggg.m_site ms on (sc.siteid = ms.siteid)  	
	order by sc.siteid, sc.visitdate; 
	
            
END;
$function$
;
