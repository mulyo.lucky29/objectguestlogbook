CREATE OR REPLACE FUNCTION gggg.fn_save_quel(
	p_quelId in bigint, 
	p_quehId in bigint,
	p_quelineno in bigint, 
	p_quetext in character varying,
	p_createdBy in character varying
 )
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
	vc 	     bigint; 
	v_result bigint;
	v_quedid bigint; 
	v_timestamp timestamp;  
begin
		SET TIMEZONE='Asia/Jakarta';	 
	 	v_timestamp = now(); 
	 	 						 
	 	 -- not exists 
	 	 if (p_quelId > 0) then
		 		-- update if existing 
	 			update gggg.m_quel 
	 			set  quehid 		    = p_quehId,
	 				 quelineno 		    = p_quelineno,
	 				 quetext 			= p_quetext, 
				 	 last_update_date 	= v_timestamp,		
				 	 last_updated_by    = p_createdBy 
	 			where quedid = p_quelId 
	 			returning quedid into v_quedid;	 	 
		 		v_result = v_quedid;	 		

	 	 else 
				insert into gggg.m_quel(						
						 quehid 
						,quelineno
						,quetext
						,creation_date
						,last_update_date 				
						,created_by 
						,last_updated_by 
				)
				values(
					    p_quehId
					   ,p_quelineno
					   ,p_quetext
					   ,v_timestamp
					   ,v_timestamp
					   ,p_createdBy
					   ,p_createdBy 
				)
				returning quedid into v_quedid;
				v_result = v_quedid;	
	 	end if; 

	    -- set output var 
    	return  v_result; 	
END;
$function$
;