CREATE OR REPLACE FUNCTION gggg.fn_delete_queh(
	p_quehId in bigint
)
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
	v_quehid bigint; 
	v_deleted_line bigint; 
 begin
		 SET TIMEZONE='Asia/Jakarta';	 

	 	if(p_quehId > 0) then
	 		-- delete line 
	 		delete from gggg.m_quel 
	 		where quehid = p_quehId;	 		
			-- delete header 
			delete from gggg.m_queh 
			where quehid = p_quehId
			returning quehid into v_quehid;			
		else 								
			v_quehid = -1;	
		end if; 
       
	    -- set output var 
    	return  v_quehid; 	
END;
$function$
;

