CREATE OR REPLACE FUNCTION gggg.fn_save_login(
	p_username in character varying,
	p_phoneNo in character varying,
	p_email	in character varying,
	p_siteid in bigint,
	p_isAdmin in character varying,	
	p_isActive in character varying,
	p_createdBy in character varying
 )
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
	vc 	     bigint; 
	v_result bigint;
	v_siteCode varchar(500); 
	v_userId bigint; 
	v_timestamp timestamp;  

 --  v_result : 
 --	 0		-> not valid 
 --	 1.. 	-> userid 
 -- -1		-> already exists 	

begin
		SET TIMEZONE='Asia/Jakarta';	 
	 	v_timestamp = now(); 
	 

	 	 -- find info lookup site code from site id 
	 	select ms.sitecode 
	 	into v_siteCode
	 	from gggg.m_site ms 
	 	where siteid = p_siteid;
	 						
	 	 -- not exists 
	 	 if not exists(
		 	select *  
		 	from gggg.m_login ml 
		 	where ml.username = p_username
		 ) then	 	
				insert into gggg.m_login(
						 username
						 --,encryptedpassword
						,phonenumber 
						,emailaddress
						,siteid 
						,sitecode 
						,isadmin
						,isactive 	 	
						,creation_date
						,last_update_date 				
						,created_by 
						,last_updated_by 
				)
				values(
						p_username
					   --,md5(p_password)	 		
					   ,p_phoneNo
					   ,p_email
					   ,p_siteId
					   ,v_siteCode
					   ,p_isAdmin
					   ,p_isActive
					   ,v_timestamp
					   ,v_timestamp
					   ,p_createdBy
					   ,p_createdBy 
				)
				returning userId into v_userId;
				v_result = v_userId;	
	 	else	 	
		 		--v_result = -1;		 	
		 		-- update if existing 
	 			update gggg.m_login
	 			set  phonenumber 		= p_phoneNo, 
				 	 emailaddress		= p_email,				 
				 	 siteid				= p_siteId,
				 	 sitecode 			= v_siteCode,
				 	 isadmin			= p_isAdmin,
				 	 isactive			= p_isActive, 	
				 	 last_update_date 	= v_timestamp,		
				 	 last_updated_by    = p_createdBy 
	 			where username = p_username
	 			returning userId into v_userId;
	 	
		 		v_result = v_userId;	 		
	 	end if; 

	    -- set output var 
    	return  v_result; 	
END;
$function$
;