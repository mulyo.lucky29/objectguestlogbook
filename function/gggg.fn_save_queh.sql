CREATE OR REPLACE FUNCTION gggg.fn_save_queh(
	p_quehId in bigint,
	p_quehName in character varying,
	p_quehDesc in character varying,
	p_createdBy in character varying
 )
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
	vc 	     bigint; 
	v_result bigint;
	v_quehId bigint; 
	v_timestamp timestamp;  
begin
		SET TIMEZONE='Asia/Jakarta';	 
	 	v_timestamp = now(); 
	 	 							 
	 	 -- not exists 
	 	 if (p_quehId > 0) then
		 		-- update if existing 
	 			update gggg.m_queh
	 			set  quehname 			= p_quehName, 
				 	 quehdesc			= p_quehDesc, 
				 	 last_update_date 	= v_timestamp,		
				 	 last_updated_by    = p_createdBy 
	 			where quehid = p_quehId 
	 			returning quehid into v_quehId;	 	 
		 		v_result = v_quehId;	 		

	 	 else 
				insert into gggg.m_queh(						
						 quehname 
						,quehdesc 
						,creation_date
						,last_update_date 				
						,created_by 
						,last_updated_by 
				)
				values(
					    p_quehName
					   ,p_quehDesc
					   ,v_timestamp
					   ,v_timestamp
					   ,p_createdBy
					   ,p_createdBy 
				)
				returning quehid into v_quehId;
				v_result = v_quehId;	
	 	end if; 

	    -- set output var 
    	return  v_result; 	
END;
$function$
;