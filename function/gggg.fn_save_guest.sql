CREATE OR REPLACE FUNCTION gggg.fn_save_guest(
	p_guestId in bigint,
	p_guestCardId in character varying, 
	p_guestName in character varying, 
	p_guestPhone in character varying, 
	p_guestEmail in character varying,
	p_guestCompany in character varying,
	p_guestPictureUrl in character varying,
	p_guestPictureCardId in character varying	
 )
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
	v_guestId bigint; 
	v_timestamp timestamp;  
 begin
	 	SET TIMEZONE='Asia/Jakarta';
	 	v_timestamp = now(); 

	 	if(p_guestId > 0) then 				
			-- update 
			update gggg.m_guest 
			set guestName    	   = p_guestName,
				guestPhone   	   = p_guestPhone,
				guestEmail   	   = p_guestEmail,
				guestCompany 	   = p_guestCompany,	
				guestPictureUrl    = p_guestPictureUrl,
				guestPictureCardId = p_guestPictureCardId,
				last_update_date   = v_timestamp
			where guestId = p_guestId
			returning guestId into v_guestId;
		else 								
			-- do insert table 
			insert into gggg.m_guest(
				guestCardId,			--1
				guestName,				--2
				guestPhone,				--3
				guestEmail, 			--4
				guestCompany,			--5
				guestPictureUrl, 		--6
				guestPictureCardId, 	--7
				isactive,				--8
				creation_date,			--9
				last_update_date		--10
			)
			values(
				p_guestCardId,			--1
				p_guestName,			--2
				p_guestPhone,			--3
				p_guestEmail, 			--4
				p_guestCompany,			--5
				p_guestPictureUrl, 		--6
				p_guestPictureCardId, 	--7
				'Y',					--8
				v_timestamp,			--9
				v_timestamp				--10
			) 
			returning guestId into v_guestId;	

		end if; 
       
	    -- set output var 
    	return  v_guestId; 	
END;
$function$
;
