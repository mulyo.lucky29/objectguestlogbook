CREATE OR REPLACE FUNCTION gggg.fn_save_assign_accesscard(
	p_visitId in bigint, 
	p_accesscardtag in character varying,
	p_accesscardpairno in character varying,
	p_user in character varying
 )
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
	v_visitid bigint = -1; 
	v_timestamp timestamp;  
 begin
		SET TIMEZONE='Asia/Jakarta';	 
	 	v_timestamp = now(); 	 	
	 		 	 
		-- update 
		update gggg.m_visit
		set last_update_date = v_timestamp,
		    last_updated_by  = p_user, 
			accesscardtag    = p_accesscardtag,
			accesscardpairno = p_accesscardpairno 
		where visitid = p_visitid  
		returning visitid into v_visitid;	
		       
	    -- set output var 
    	return  v_visitid; 	
END;
$function$
;