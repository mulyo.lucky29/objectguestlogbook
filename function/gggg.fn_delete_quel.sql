CREATE OR REPLACE FUNCTION gggg.fn_delete_quel(
	p_quelId in bigint
)
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
	v_quelId bigint; 
	v_deleted_line bigint; 
 begin
		 SET TIMEZONE='Asia/Jakarta';	 

	 	if(p_quelId > 0) then
	 		-- delete line 
	 		delete from gggg.m_quel 
	 		where quedid = p_quelId	 		
			returning quedid into v_quelId;			
		else 								
			v_quelId = -1;	
		end if; 
       
	    -- set output var 
    	return  v_quelId; 	
END;
$function$
;
