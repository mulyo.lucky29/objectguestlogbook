CREATE OR REPLACE FUNCTION gggg.fn_save_contact(
	p_contactid in bigint, 
	p_sitecode in character varying,
	p_sitespecific in character varying,
	p_tenantcompany in character varying,
	p_contacttype in character varying,	
	p_contactname in character varying, 
	p_contactphone in character varying,
	p_contactemail in character varying,
	p_contactext in character varying,
	p_user in character varying
 )
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
 declare 
	v_contactid bigint; 
	v_timestamp timestamp;  
 begin
		 SET TIMEZONE='Asia/Jakarta';	 
	 	v_timestamp = now(); 
	 
	 	if(p_contactid > 0) then 				
			-- update 
			update gggg.m_contact 
			set siteCode    	   = p_sitecode,
				siteSpecific   	   = p_sitespecific,
				tenantcompany 	   = p_tenantcompany,
				contacttype 	   = p_contacttype,				
				contactname   	   = p_contactname,
				contactemail 	   = p_contactemail,
				contactphone 	   = p_contactphone,				
				contactext    	   = p_contactext,
				last_updated_by    = p_user,
				last_update_date   = v_timestamp
			where contactId = p_contactid
			returning contactId into v_contactid;
		else 								
			-- do insert table 
			insert into gggg.m_contact(
				siteCode,				--1
				siteSpecific,			--2
				tenantcompany,			--3 
				contacttype,			--4
				contactname,			--5				
				contactemail, 			--6
				contactphone,			--7
				contactext,				--8
				creation_date,			--9
				created_by,				--10
				last_update_date,		--11
				last_updated_by			--12
			)
			values(
				p_sitecode,				--1
				p_sitespecific,			--2
				p_tenantcompany,		--3
				p_contacttype,			--4				
				p_contactname,			--5				
				p_contactemail, 		--6
				p_contactphone,			--7 				
				p_contactext,			--8				
				v_timestamp,			--9
				p_user,					--10
				v_timestamp,			--11
				p_user					--12
			) 
			returning contactId into v_contactid;	

		end if; 
       
	    -- set output var 
    	return  v_contactid; 	
END;
$function$
;