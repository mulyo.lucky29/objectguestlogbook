CREATE TABLE gggg.m_contact (
	contactId numeric NOT NULL DEFAULT nextval('gggg.m_contactId_seq'),
	siteCode varchar(50) NOT NULL,
	siteSpecific varchar(500) NOT NULL, 
	tenantCompany varchar(500) NULL,
	contactType varchar(500) NULL,
	contactName varchar(500) NOT NULL,	
	contactEmail varchar(500) NULL,
	contactPhone varchar(50) NULL,
	contactExt varchar(50) NOT NULL,	
	creation_date timestamp(0) NULL,
	created_by varchar(500) NULL,
	last_update_date timestamp(0) NULL,
	last_updated_by varchar(500) NULL
);

