CREATE TABLE gggg.m_login (    
	userId numeric NOT NULL DEFAULT nextval('gggg.m_userId_seq'),
	userName varchar(50) NOT NULL,
	encryptedPassword varchar(50) NULL,	
	phoneNumber varchar(50) NULL, 
	emailAddress varchar(50) NULL,
	siteId numeric NOT NULL,
	siteCode varchar(50) NOT NULL,
	isAdmin varchar(1) NOT NULL,
	isActive varchar(1) NOT NULL,
	last_login_date timestamp(0) NULL,
	creation_date timestamp(0) NULL,
	created_by varchar(50) NULL,
	last_update_date timestamp(0) NULL,
	last_updated_by varchar(50) NULL,
	CONSTRAINT m_login_unique UNIQUE (userName)
);
