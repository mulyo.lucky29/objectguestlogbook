CREATE TABLE gggg.m_queH (
	queHId numeric NOT NULL DEFAULT nextval('gggg.m_queHid_seq'),
	queHName varchar(500),
	queHDesc varchar(2000), 	
	creation_date timestamp(0) NULL,
	created_by varchar(50) NULL,
	last_update_date timestamp(0) NULL,
	last_updated_by varchar(50) NULL
);
