CREATE TABLE gggg.m_queL (
	queDId numeric NOT NULL DEFAULT nextval('gggg.m_queLId_seq'),
	queHId numeric NOT null,
	queLineNo numeric NOT null,
	queText varchar(2000) NOT null,
	creation_date timestamp(0) NULL,
	created_by varchar(50) NULL,
	last_update_date timestamp(0) NULL,
	last_updated_by varchar(50) NULL
);
