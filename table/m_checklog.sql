CREATE TABLE gggg.m_checklog (
	checkLogId NUMERIC NOT NULL DEFAULT nextval('gggg.m_checkLogId_seq'),
	visitId numeric NOT NULL,
	visitSessionCode varchar(500) NULL,
	checkedTime timestamp(0) null,
	checkTyped varchar(50) null, 
	checkedTerminalId varchar(50) NULL
);
