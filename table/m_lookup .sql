CREATE TABLE gggg.m_lookup (
	lookupId numeric NOT NULL DEFAULT nextval('gggg.m_lookupid_seq'),
	lookupGroupCode varchar(500) NOT NULL,
	lookupValue varchar(500) NOT NULL,
	lookupDescription varchar(500) NOT NULL,
	lookupOrderNo numeric NOT NULL,
	isActive varchar(1) NOT NULL,
	creation_date timestamp(0) NULL,
	created_by varchar(50) NULL,
	last_update_date timestamp(0) NULL,
	last_updated_by varchar(50) NULL,
	CONSTRAINT m_lookup_unique UNIQUE (lookupGroupCode,lookupValue)
);
